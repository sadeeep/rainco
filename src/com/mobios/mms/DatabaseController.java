package com.mobios.mms;

import java.sql.Connection;
import java.sql.DriverManager;

public class DatabaseController
{
  private static DatabaseController instance;
  
  private DatabaseController() {}
  
  public static DatabaseController getInstance()
  {
    if (instance == null) {
      synchronized (DatabaseController.class) {
        if (instance == null) {
          instance = new DatabaseController();
        }
      }
    }
    return instance;
  }
  
  public Connection getMysqlConnection() {
    Connection con = null;
    
    String dbUrl = "jdbc:mysql://localhost:3306/reload";
    String dbUser = "root";
    
    String dbPass = "root";
    try
    {
      Class.forName("com.mysql.jdbc.Driver").newInstance();
      return DriverManager.getConnection(dbUrl, dbUser, dbPass);
    }
    catch (Exception e) {
      System.out.println("error : " + e.toString());
    }
    return con;
  }
}
