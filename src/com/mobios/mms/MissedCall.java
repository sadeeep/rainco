package com.mobios.mms;

public class MissedCall {
  private String date_time;
  private String hour_value;
  private int no_count;
  
  public MissedCall() {}
  
  public String getDate_time() { return date_time; }
  
  public void setDate_time(String date_time) {
    this.date_time = date_time;
  }
  
  public String getHour_value() { return hour_value; }
  
  public void setHour_value(String hour_value) {
    this.hour_value = hour_value;
  }
  
  public int getNo_count() { return no_count; }
  
  public void setNo_count(int no_count) {
    this.no_count = no_count;
  }
}
