package com.mobios.mms;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.mobios.mms.FALMsg;
import com.mobios.mms.MissedCall;
import com.mobios.mms.Panadol;
import com.mobios.mms.knorMF;
import com.mobios.mms.knorrMFDB;

public class dashboardController {
	
	/*Custom method for get summery of the campaign*/
    public static List<Integer> getSummeryFromMysqlDB(List<String> querylist){

        List<Integer> countlist = new ArrayList<Integer>();
        Connection connection = null;
			DatabaseController d = DatabaseController.getInstance();
			connection = d.getMysqlConnection();

       Statement stmt = null;
       ResultSet rs=null;

       try {
            stmt = (Statement) connection.createStatement();

            for(String SQL :querylist){
            rs = stmt.executeQuery(SQL);
            while (rs.next()){
               int count=Integer.parseInt(rs.getString("count"));
                countlist.add(count);

            }
        }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    finally{
            try {
                connection.close();
                stmt.close();
            }catch (SQLException e) {

                e.printStackTrace();
            }
    }

    return countlist;

    }
    
    /* get message count by date*/
    public static List<Integer> getSummeryFromMysqlDBByDate( List<String> querylist){

        List<Integer> countlist = new ArrayList<Integer>();
        Connection connection = null;
        DatabaseController d = DatabaseController.getInstance();
		connection = d.getMysqlConnection();

        Statement stmt = null;
        ResultSet rs=null;
        try {
            stmt = (Statement) connection.createStatement();
            for(String SQL :querylist){
                rs = stmt.executeQuery(SQL);
                while (rs.next()){
                    int count=Integer.parseInt(rs.getString("count"));
                    countlist.add(count);

                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally{
                try {
                    connection.close();
                    stmt.close();
                }catch (SQLException e) {
                    e.printStackTrace();
                }
        }

        return countlist;

    }
    
    /*custom pie chart-data get from mysql database*/
    public static List<Map> getPieChatData(String campaignId,String stDate,String endDate,List<String> querylist){
        List<Map> responseMap = new ArrayList<Map>();
        Connection connection = null;
        DatabaseController d = DatabaseController.getInstance();
		connection = d.getMysqlConnection();

        Statement stmt = null;
        ResultSet rs=null;
        Map smsCountMap = new LinkedHashMap();
        List<Map> smsCountArray = new ArrayList<Map>();
        int operatorTotalCount=0;

 
        try {
            stmt = (Statement) connection.createStatement();
            for(int i=0;i<querylist.size();i++){
                rs = stmt.executeQuery(querylist.get(i));
                while (rs.next()){
                    int count=Integer.parseInt(rs.getString("no_count"));

                    switch(i){
                        case 0 :
                            Map dialogMap = new LinkedHashMap();
                            dialogMap.put("operator", "DIALOG");
                            dialogMap.put("count", count);
                            responseMap.add(dialogMap);
                            operatorTotalCount=operatorTotalCount + count;

                            break; //optional
                        case 1 :
                            Map mobitelMap = new LinkedHashMap();
                            mobitelMap.put("operator","MOBITEL");
                            mobitelMap.put("count", count);
                            responseMap.add(mobitelMap);
                            operatorTotalCount=operatorTotalCount + count;

                            break; //optional
                        case 2 :
                            Map hutchMap = new LinkedHashMap();
                            hutchMap.put("operator", "HUTCH");
                            hutchMap.put("count", count);
                            responseMap.add(hutchMap);
                            operatorTotalCount=operatorTotalCount + count;

                            break; //optional

                        case 3 :
                            Map airtelMap = new LinkedHashMap();
                            airtelMap.put("operator", "AIRTEL");
                            airtelMap.put("count", count);
                            responseMap.add(airtelMap);
                            operatorTotalCount=operatorTotalCount + count;


                            break; //optional

                        case 4 :
                            Map etisalatMap = new LinkedHashMap();
                            etisalatMap.put("operator", "ETISALAT");
                            etisalatMap.put("count", count);
                            responseMap.add(etisalatMap);
                            operatorTotalCount=operatorTotalCount + count;


                            break; //optional
                        case 5 :
                            Map otherMap = new LinkedHashMap();
                            otherMap.put("operator", "OTHER");
                            otherMap.put("count", count-operatorTotalCount);
                            responseMap.add(otherMap);

                            break; //optional
                        default :

                    }


                }
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally{
            try {
                connection.close();
                stmt.close();
            }catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return responseMap;

    }
    
    
    /*custom bar chart-data get from mysql database*/
    public static List<Map> getBarChatData(String campaignId,String stDate,String endDate,String query){
        List<Map> responseMap = new ArrayList<Map>();
        Connection connection = null;
        DatabaseController d = DatabaseController.getInstance();
		connection = d.getMysqlConnection();

        Statement stmt = null;
        ResultSet rs=null;
 
        try {
            stmt = (Statement) connection.createStatement();
                rs = stmt.executeQuery(query);
                while (rs.next()){
                    int count=Integer.parseInt(rs.getString("no_count"));
                    String date_time=""+rs.getString("date_time");
                            Map countMap = new LinkedHashMap();
                            countMap.put("date_time", date_time);
                            countMap.put("count", count);
                            responseMap.add(countMap);

                }


        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally{
            try {
                connection.close();
                stmt.close();
            }catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return responseMap;

    }
    
    public static List<Map> addEmptyValuesToBarChart(String SQL){
    	List<Map> responseMap = new ArrayList<Map>();
    	DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    	//MissedCall msc= new MissedCall();
    	List<MissedCall> missedCallList = new ArrayList<MissedCall>();
    	List<MissedCall> missedCallListAll = new ArrayList<MissedCall>();
    	List<String> dateList = new ArrayList<String>();
    	
    	Connection connection = null;
        DatabaseController d = DatabaseController.getInstance();
		connection = d.getMysqlConnection();

        Statement stmt = null;
        ResultSet rs=null;
 
        try {
            stmt = (Statement) connection.createStatement();
                rs = stmt.executeQuery(SQL);
                while (rs.next()){
                	MissedCall msc= new MissedCall();
                    int count=Integer.parseInt(rs.getString("no_count"));
                    String date_time=""+rs.getString("date_time");
                    String hour_val=""+rs.getString("hour_value");
                       msc.setDate_time(date_time);    
                       msc.setHour_value(hour_val);
                       msc.setNo_count(count);
                    missedCallList.add(msc);
                }


        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally{
            try {
                connection.close();
                stmt.close();
            }catch (SQLException e) {
                e.printStackTrace();
            }
        }
        String campaignStDate=missedCallList.get(0).getDate_time();
        String campaignEndDate=missedCallList.get(missedCallList.size()-1).getDate_time();
        String comparingDate=campaignStDate;
        DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
        Date fromDate=null;
        Date toDate=null;
		try {
			fromDate = format2.parse(campaignStDate);
			toDate =format2.parse(campaignEndDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
		System.out.println("dateList fromDate..............."+fromDate);
		System.out.println("dateList fromDate..............."+toDate);
    	Calendar cal = Calendar.getInstance();
    	cal.setTime(fromDate);
    	cal.add(Calendar.DATE, 0);
    	dateList.add(format2.format(cal.getTime()));
    	while (cal.getTime().before(toDate)) {
    	    cal.add(Calendar.DATE, 1);
    	    dateList.add(format2.format(cal.getTime()));
    	    //System.out.println(format2.format(cal.getTime()));
    	}
    	
    	 for(int i=0;i<dateList.size();i++){
    		 for(int j=0;j<24;j++){
    			 MissedCall msc= new MissedCall();
    			 msc.setDate_time(dateList.get(i));
    			 msc.setHour_value(""+j);
    			 msc.setNo_count(0);
    			 missedCallListAll.add(msc);
    		 }
    	 }
    	 System.out.println("dateList size 11..............."+dateList.size());
    	 System.out.println("missedCallList size 11..............."+missedCallList.size());
    	 System.out.println("missedCallListAll size 11..............."+missedCallListAll.size());
    	 
       
        	 for(int j=0;j<missedCallListAll.size();j++){
        		 for(int i=0;i<missedCallList.size();i++){
             	if(missedCallListAll.get(j).getDate_time().equals(missedCallList.get(i).getDate_time()) && missedCallListAll.get(j).getHour_value().equals(missedCallList.get(i).getHour_value())){
             		
             		 missedCallListAll.get(j).setNo_count(missedCallList.get(i).getNo_count());
             		
             	}
             }

        }
        	 	System.out.println("missedCallListAll size 22..............."+missedCallListAll.size());
    	for(int i=0;i<missedCallListAll.size();i++){
    		
    		Map countMap = new LinkedHashMap();
            countMap.put("date_time", missedCallListAll.get(i).getDate_time()+" "+missedCallListAll.get(i).getHour_value()+" 00 00" );
            countMap.put("count", missedCallListAll.get(i).getNo_count());
            responseMap.add(countMap);
  		
    	}
    	return responseMap;
    	
    }
    
    //get audio file list
    public static List<Map> getAudioFilelist(String campaignId,String stDate,String endDate,String query,String host){
        List<Map> responseMap = new ArrayList<Map>();
        Connection connection = null;
        DatabaseController d = DatabaseController.getInstance();
		connection = d.getMysqlConnection();
		
        Statement stmt = null;
        ResultSet rs=null;
        
        try {
            stmt = (Statement) connection.createStatement();
                rs = stmt.executeQuery(query);
                while (rs.next()){
                	String msisdn=""+rs.getString("msisdn");
                    String date_time=""+rs.getString("date_time");
                    String file_path=""+rs.getString("file_path");
                    String fileName=""+rs.getString("file_name");
                    String status=""+rs.getString("info1");
                    String id=""+rs.getString("id");
                    Map countMap = new LinkedHashMap();
                    if(status.equals("null")){
                    	status="";
                    	System.out.println("*null*******"+status);
                    	//countMap.put("btnClass", "btn btn-info disabled");
                    }else{
                    	status="checked=true";
                    	System.out.println("*true******"+status);
                    	//countMap.put("btnClass", "btn btn-info");
                    }

                    		countMap.put("btnClass", "btn btn-info disabled");
                            countMap.put("msisdn", msisdn);
                            countMap.put("date_time", date_time);
                            countMap.put("id", id);
                            countMap.put("file_path",host+file_path+fileName);
                            countMap.put("status", status);//
                            responseMap.add(countMap);

                }


        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally{
            try {
                connection.close();
                stmt.close();
            }catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return responseMap;

    }
    
    ////////////////////Horlicks record files///////////////////////
    
    public static List<Map> getAudioFilelistHorlicks(String campaignId,String stDate,String endDate,String query,String host){
        List<Map> responseMap = new ArrayList<Map>();
        Connection connection = null;
        DatabaseController d = DatabaseController.getInstance();
		connection = d.getMysqlConnection();
		
        Statement stmt = null;
        ResultSet rs=null;
        
        try {
            stmt = (Statement) connection.createStatement();
                rs = stmt.executeQuery(query);
                while (rs.next()){
                	String msisdn=""+rs.getString("msisdn");
                    String date_time=""+rs.getString("date_time");
                    String file_path=""+rs.getString("file_path");
                    String fileName=""+rs.getString("file_name");
                    String status=""+rs.getString("info1");
                    String id=""+rs.getString("id");
                    Map countMap = new LinkedHashMap();
                    /*if(status.equals("null")){
                    	status="";
                    	System.out.println("*null*******"+status);
                    	//countMap.put("btnClass", "btn btn-info disabled");
                    }else{
                    	status="checked=true";
                    	System.out.println("*true******"+status);
                    	//countMap.put("btnClass", "btn btn-info");
                    }*/

                    		//countMap.put("btnClass", "btn btn-info disabled");
                            countMap.put("msisdn", msisdn);
                            countMap.put("date_time", date_time);
                            countMap.put("id", id);
                            countMap.put("file_path",host+file_path+fileName);
                            countMap.put("status", status);//
                            responseMap.add(countMap);

                }


        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally{
            try {
                connection.close();
                stmt.close();
            }catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return responseMap;

    }
    
    //update saved status in calles_recodes.
    public static boolean updateStatus(String campaignId,String stDate,String endDate,String query){
        Connection connection = null;
        DatabaseController d = DatabaseController.getInstance();
		connection = d.getMysqlConnection();
        Statement stmt = null;
        ResultSet rs=null;
        boolean b= false;
        
        try {
            stmt = (Statement) connection.createStatement();
            stmt.executeUpdate(query);
            b=true;

        } catch (SQLException e) {
            e.printStackTrace();
            b=false;
        }
        finally{
            try {
                connection.close();
                stmt.close();
            }catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return b;

    }
    
    public List<knorMF> getMSISDNList(String SQL){
    	List<knorMF> panadolList = new ArrayList<knorMF>();
        Connection connection = null;
        DatabaseController d = DatabaseController.getInstance();
		connection = d.getMysqlConnection();
		
        Statement stmt = null;
        ResultSet rs=null;
        
        try {
            stmt = (Statement) connection.createStatement();
                rs = stmt.executeQuery(SQL);
                while (rs.next()){
                	
                	knorMF panadol = new knorMF();
                    panadol.setDate_time(rs.getString("date_time"));
                    panadol.setInfo1(rs.getString("msisdn"));
                    panadol.setInfo2(rs.getString("msg"));
                    panadol.setNaoffnet(rs.getString("msg_type"));
                    panadolList.add(panadol); 
                    
                }
                }catch (Exception e) {
					e.printStackTrace();
                }
        finally{
            try {
                connection.close();
                stmt.close();
            }catch (SQLException e) {
                e.printStackTrace();
            }
        }
    	
    	return panadolList;
    }
    
    ////////////////////////////////////////////////////////////
    
    public List<FALMsg> getAddressList(String SQL){
    	List<FALMsg> FALMsgList = new ArrayList<FALMsg>();
        Connection connection = null;
        DatabaseController d = DatabaseController.getInstance();
		connection = d.getMysqlConnection();
		
        Statement stmt = null;
        ResultSet rs=null;
        
        try {
            stmt = (Statement) connection.createStatement();
                rs = stmt.executeQuery(SQL);
                while (rs.next()){
                	
                	FALMsg falMsg = new FALMsg();
                	falMsg.setDate_time(rs.getString("date_time"));
                	falMsg.setCli(rs.getString("cli"));
                	falMsg.setMsg(rs.getString("msg"));
                    FALMsgList.add(falMsg);
                    
                }
                }catch (Exception e) {
                	System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$"+e.toString());
					e.printStackTrace();
                }
        finally{
        	if ( connection != null ){
				try{ 
					connection.close(); 
				}catch(Exception ce){
					ce.printStackTrace();
					}
			}
        }
    	
    	return FALMsgList;
    }
    
   
    
    public List<knorMF> getonnetOffNet(List<String> SQL){
		List<knorrMFDB> KnorMFmsgCount = new ArrayList<knorrMFDB>();
		List<knorMF> KnorMFmsg = new ArrayList<knorMF>();
		Connection connection = null;
		DatabaseController d = DatabaseController.getInstance();
		connection = d.getMysqlConnection();

		Statement stmt = null;
		ResultSet rs = null;
           
		try {
			stmt = (Statement) connection.createStatement();
			for(String query:SQL){
			rs = stmt.executeQuery(query);
				while (rs.next()) {
					// COUNT DATE(date_time) status msg_type
					knorrMFDB knormMfCount = new knorrMFDB();
					knormMfCount.setDate_time(rs.getString("DATE(date_time)"));
					knormMfCount.setCount(rs.getString("COUNT"));
					knormMfCount.setMsg_type(rs.getString("msg_type"));
					knormMfCount.setStatus(rs.getString("status"));
										
					KnorMFmsgCount.add(knormMfCount);
	
				}
			}
						
		} catch (Exception e) {
			System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$" + e.toString());
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (Exception ce) {
					ce.printStackTrace();
				}
			}
		}
		
		for(int i=0;i<(KnorMFmsgCount.size()/4);i++){
			knorMF knorrmfob = new knorMF();
			knorrmfob.setDate_time(KnorMFmsgCount.get(i).getDate_time());
			//KnorMFmsg 
			for(int j=0;j<KnorMFmsgCount.size();j++){
				if(KnorMFmsgCount.get(i).getDate_time().equals(KnorMFmsgCount.get(j).getDate_time())){
					
					if(KnorMFmsgCount.get(j).getStatus().equals("onnet") && KnorMFmsgCount.get(j).getMsg_type().equals("nameAddress")){
						knorrmfob.setNaOnnet(KnorMFmsgCount.get(j).getCount());
					}
					else if(KnorMFmsgCount.get(j).getStatus().equals("onnet") && KnorMFmsgCount.get(j).getMsg_type().equals("thanks")){
						knorrmfob.setThOnnet(KnorMFmsgCount.get(j).getCount());
					}
					else if(KnorMFmsgCount.get(j).getStatus().equals("offnet") && KnorMFmsgCount.get(j).getMsg_type().equals("nameAddress")){
						knorrmfob.setNaoffnet(KnorMFmsgCount.get(j).getCount());
					}
					else if(KnorMFmsgCount.get(j).getStatus().equals("offnet") && KnorMFmsgCount.get(j).getMsg_type().equals("thanks")){
						//System.out.println("$$$$$$$$$$$$"+j+"$$$$$$$$$$$$$" +KnorMFmsgCount.get(j).getStatus());
						//System.out.println("$$$$$$$$$$$$"+j+"$$$$$$$$$$$$$" +KnorMFmsgCount.get(j).getMsg_type());
						knorrmfob.setThoffnet(KnorMFmsgCount.get(j).getCount());
					}
					if(knorrmfob.getNaoffnet()!=null && knorrmfob.getNaOnnet()!=null && knorrmfob.getThOnnet()!=null && knorrmfob.getThOnnet()!=null){
						
						System.out.println("$j:"+j+"$Naoffnet" +knorrmfob.getNaoffnet()+"$getNaOnnet" +knorrmfob.getNaOnnet()+"$getThoffnet" +knorrmfob.getThoffnet()+"$getThOnnet" +knorrmfob.getThOnnet());
						//System.out.println("$$$$$$$$$$$$"+j+"$$$$$$$$$$$$$" +knorrmfob.getNaOnnet());
						//System.out.println("$$$$$$$$$$$$"+j+"$$$$$$$$$$$$$" +knorrmfob.getThoffnet());
						//System.out.println("$$$$$$$$$$$$"+j+"$$$$$$$$$$$$$" +knorrmfob.getThOnnet());
						KnorMFmsg.add(knorrmfob);
					}
				}
				
			}
			
			
		}
		System.out.println("$$$$$$$$$$$KnorMFmsg.size() $$$$$$$$$$$$$$" + KnorMFmsg.size());

       	return KnorMFmsg;
       }
    
    public List<String> getUniqueNumberList(String SQL){
    	List<String> uniqueNum = new ArrayList<String>();
        Connection connection = null;
        DatabaseController d = DatabaseController.getInstance();
		connection = d.getMysqlConnection();
		
        Statement stmt = null;
        ResultSet rs=null;
        
        try {
            stmt = (Statement) connection.createStatement();
                rs = stmt.executeQuery(SQL);
                while (rs.next()){
                	
                  
                	uniqueNum.add(rs.getString("msisdn"));
                    
                }
                }catch (Exception e) {
					e.printStackTrace();
                }
        finally{
            try {
                connection.close();
                stmt.close();
            }catch (SQLException e) {
                e.printStackTrace();
            }
        }
    	
    	return uniqueNum;
    }
    public ResultSet getLatestCampaign( ){
    	
        Connection connection = null;
        DatabaseController d = DatabaseController.getInstance();
		connection = d.getMysqlConnection();
		String sdate="";
        Statement stmt = null;
        ResultSet rs=null;
        Statement stmt1 = null;
        ResultSet rs1=null;
        String SQL="SELECT MAX(`start_date`) FROM `campains`;";
              try {
            stmt = (Statement) connection.createStatement();
                rs = stmt.executeQuery(SQL);
          while(rs.next()) {
        		sdate = rs.getString("MAX(`start_date`)");
        
        	        	String sql1 = "SELECT * FROM  `campains` WHERE start_date='"+sdate+"';";		
        	        	   stmt1 = (Statement) connection.createStatement();
        	                rs1 = stmt1.executeQuery(sql1);
        	               
        		
        		
          }
       
                }catch (Exception e) {
                	System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$"+e.toString());
					e.printStackTrace();
                }
      
       
    	
              return rs1;    
    }  
    
    public ResultSet getListCampaign( ){
    	
        Connection connection = null;
        DatabaseController d = DatabaseController.getInstance();
		connection = d.getMysqlConnection();
		int id=0;
        Statement stmt = null;
        ResultSet rs=null;
        Statement stmt1 = null;
        ResultSet rs1=null;
        String SQL="SELECT * FROM `campains`;";
              try {
            stmt = (Statement) connection.createStatement();
                rs = stmt.executeQuery(SQL);
        
       
                }catch (Exception e) {
                	System.out.println("$$$$$$$$$$$$2$$$$$$$$$$$$"+e.toString());
					e.printStackTrace();
                }
      
       
    	
              return rs;    
    }  
public String getcurent_page( ){
    	
        Connection connection = null;
        DatabaseController d = DatabaseController.getInstance();
		connection = d.getMysqlConnection();
		String page="";
        Statement stmt = null;
        ResultSet rs=null;
      
        String SQL="SELECT page_name FROM `campains`;";
              try {
            stmt = (Statement) connection.createStatement();
                rs = stmt.executeQuery(SQL);
        page=rs.getString("page_name");
       
                }catch (Exception e) {
                	System.out.println("$$$$$$$$$$$$2$$$$$$$$$$$$"+e.toString());
					e.printStackTrace();
                }
      
       
    	
              return page;    
    }  
}
