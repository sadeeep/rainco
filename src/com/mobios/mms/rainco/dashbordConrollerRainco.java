package com.mobios.mms.rainco;

import com.mobios.mms.DatabaseController;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class dashbordConrollerRainco {
	
	public dashbordConrollerRainco() {
	}

	public ArrayList<TownList> getTownList() {
		Statement statement = null;
		TownList townList = null;
		Connection connection = null;
		ArrayList<TownList> arrList = new ArrayList();
		try {
			DatabaseController d = DatabaseController.getInstance();
			connection = d.getMysqlConnection();
			String query = "SELECT DISTINCT `town` FROM `rainco_secret_code` ORDER BY town ASC";

			ResultSet rs = null;
			statement = connection.createStatement();
			rs = statement.executeQuery(query);
			System.out.println("SQL " + query);
			while (rs.next()) {
				townList = new TownList();
				townList.setTwon(rs.getString("town"));

				arrList.add(townList);
			}
		} catch (SQLException e) {
			e.printStackTrace();

			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return arrList;
	}

	public java.util.List<Outlet> getOutletList(String town) {
		Connection con = null;
		Statement st = null;
		java.util.List<Outlet> outletlist = new ArrayList();

		String SQL = "SELECT DISTINCT `outlet_code` FROM `rainco_secret_code` WHERE `town` = '" + town + "'";

		System.out.println("SQL outlet List  : " + SQL);
		try {
			DatabaseController d = DatabaseController.getInstance();
			con = d.getMysqlConnection();
			ResultSet rs = null;
			st = con.createStatement();
			rs = st.executeQuery(SQL);
			while (rs.next()) {
				Outlet outlet = new Outlet();
				outlet.setOutlet(rs.getString("outlet_code"));
				outletlist.add(outlet);
			}
		} catch (Exception e) {
			System.out.println("Error On distributor List  : " + e.toString());
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException localSQLException1) {
				}
				con = null;
			}
		}
		System.out.println("outlet List  in db con : " + outletlist);
		return outletlist;
	}

	public java.util.List<raicoList> getRedeemedMSISDNList(String SQL, String Case) {
		java.util.List<raicoList> raicoListArr = new ArrayList();
		Connection connection = null;
		DatabaseController d = DatabaseController.getInstance();
		connection = d.getMysqlConnection();

		Statement stmt = null;
		ResultSet rs = null;

		try {
			stmt = connection.createStatement();
			rs = stmt.executeQuery(SQL);
			while (rs.next()) {
				raicoList panadol = new raicoList();
				if (Case.equals("OTP")) {
					panadol.setDate_time(rs.getString("date_time"));
					panadol.setMsisdn(rs.getString("msisdn"));
					panadol.setOtp(rs.getString("pin_no"));
					panadol.setOtpAttempts(rs.getInt("attempts"));
					panadol.setStatus(rs.getString("status"));
				} else if (Case.equals("BLACK")) {
					panadol.setDate_time(rs.getString("inserted_date"));
					panadol.setMsisdn(rs.getString("msisdn"));
					panadol.setOtpAttempts(rs.getInt("attempts"));
					panadol.setStatus(rs.getString("status"));
				} else if (Case.equals("SCT")) {
					panadol.setDate_time(rs.getString("date_time"));
					panadol.setMsisdn(rs.getString("msisdn"));
					panadol.setSecretCode(rs.getString("secret_code"));
					panadol.setSecretAttempts(rs.getInt("code_attempts"));
					panadol.setStatus(rs.getString("status"));
				} else {
					panadol.setDate_time(rs.getString("date_time"));
					panadol.setMsisdn(rs.getString("msisdn"));
					panadol.setOtp(rs.getString("pin_no"));
					panadol.setStatus(rs.getString("status"));

					panadol.setReloadAmount(rs.getString("reload_amount"));
					panadol.setReloadSt(rs.getString("reload_status"));
					panadol.setOutletname(rs.getString("outlet_name"));
					panadol.setTown(rs.getString("town"));
					panadol.setSecretCode(rs.getString("secret_code"));
					panadol.setOutletId(rs.getString("outlet_code"));
					panadol.setOtpAttempts(rs.getInt("attempts"));
					panadol.setSecretAttempts(rs.getInt("code_attempts"));
				}
				raicoListArr.add(panadol);
			}
		} catch (Exception e) {
			e.printStackTrace();

			try {
				connection.close();
				stmt.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			try {
				connection.close();
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return raicoListArr;
	}
}
