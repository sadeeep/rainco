package com.mobios.mms.rainco;

public class raicoList {
  private String date_time;
  private String msisdn;
  private String secretCode;
  private String otp;
  private String status;
  private String dateTime;
  private String reloadAmount;
  private String outletId;
  private String outletname;
  private String town;
  private String reloadSt;
  private String serialNo;
  private String info1;
  private String info2;
  private int otpAttempts;
  private int secretAttempts;
  
  public raicoList() {}
  
  public int getOtpAttempts() { return otpAttempts; }
  
  public void setOtpAttempts(int otpAttempts) {
    this.otpAttempts = otpAttempts;
  }
  
  public int getSecretAttempts() { return secretAttempts; }
  
  public void setSecretAttempts(int secretAttempts) {
    this.secretAttempts = secretAttempts;
  }
  
  public String getDate_time() { return date_time; }
  
  public void setDate_time(String date_time) {
    this.date_time = date_time;
  }
  
  public String getMsisdn() { return msisdn; }
  
  public void setMsisdn(String msisdn) {
    this.msisdn = msisdn;
  }
  
  public String getSecretCode() { return secretCode; }
  
  public void setSecretCode(String secretCode) {
    this.secretCode = secretCode;
  }
  
  public String getOtp() { return otp; }
  
  public void setOtp(String otp) {
    this.otp = otp;
  }
  
  public String getStatus() { return status; }
  
  public void setStatus(String status) {
    this.status = status;
  }
  
  public String getDateTime() { return dateTime; }
  
  public void setDateTime(String dateTime) {
    this.dateTime = dateTime;
  }
  
  public String getReloadAmount() { return reloadAmount; }
  
  public void setReloadAmount(String reloadAmount) {
    this.reloadAmount = reloadAmount;
  }
  
  public String getOutletId() { return outletId; }
  
  public void setOutletId(String outletId) {
    this.outletId = outletId;
  }
  
  public String getOutletname() { return outletname; }
  
  public void setOutletname(String outletname) {
    this.outletname = outletname;
  }
  
  public String getTown() { return town; }
  
  public void setTown(String town) {
    this.town = town;
  }
  
  public String getReloadSt() { return reloadSt; }
  
  public void setReloadSt(String reloadSt) {
    this.reloadSt = reloadSt;
  }
  
  public String getSerialNo() { return serialNo; }
  
  public void setSerialNo(String serialNo) {
    this.serialNo = serialNo;
  }
  
  public String getInfo1() { return info1; }
  
  public void setInfo1(String info1) {
    this.info1 = info1;
  }
  
  public String getInfo2() { return info2; }
  
  public void setInfo2(String info2) {
    this.info2 = info2;
  }
}
