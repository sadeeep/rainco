package com.mobios.mms;

public class FALMsg {
  private String date_time;
  private String cli;
  private String msg;
  
  public FALMsg() {}
  
  public String getDate_time() { return date_time; }
  
  public void setDate_time(String date_time) {
    this.date_time = date_time;
  }
  
  public String getCli() { return cli; }
  
  public void setCli(String cli) {
    this.cli = cli;
  }
  
  public String getMsg() { return msg; }
  
  public void setMsg(String msg) {
    this.msg = msg;
  }
}
