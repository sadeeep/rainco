package com.mobios.mms;

import java.sql.Connection;
import java.sql.ResultSet;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.mobios.mms.MainController;
import com.mobios.mms.DatabaseController;
import java.sql.Statement;



public class MainController {
	
public static final MainController maincon_instance = new MainController();
	
	public static MainController getInstance() {
		
		return maincon_instance;
		
	}
	
	
	/**
	 * retrieve data hourly in a day
	 * @param query
	 * @param min
	 * @param max
	 * @return
	 */
	public String getData(String query, String min, String max){
		JsonObject jsonObject = new JsonObject();
		
		Connection conn = null;
		Statement st = null;
		String json = null;
		int minn = Integer.parseInt(min);
		int maxx = Integer.parseInt(max);

		try {		
			
			/*for(int i=minn; i<=maxx; i++){
				jsonObject.addProperty(String.valueOf(i), "0");
			}*/
			
			for(int i=1; i<=24; i++){
				jsonObject.addProperty(String.valueOf(i), "0");
			}
			
			System.out.println("jsonObject : "+jsonObject.toString());
			
			conn = DatabaseController.getInstance().getMysqlConnection();
			if (conn == null) {
				
				return null;
			}
			
			ResultSet rs = null;
			st = (Statement) conn.createStatement();
			rs = st.executeQuery(query);
			
			while (rs.next()) {
				//rs.getString("t_hour")+":"+rs.getString("count")
				jsonObject.addProperty(rs.getString("t_hour"), rs.getString("count"));
	
			}		
			
			json = jsonObject.toString();
			System.out.println("json : "+json);
			
			rs.close();
			st.close();
			
		}catch(Exception e){
			System.out.println(e);
			//LogUtil.getErrorLog().debug(uuid+",ERROR,class=com.mobios.vaseline.controllers.MainController,method=isToMsisdnAvailable,exception="+e);
			//LogUtil.getEventLog().debug(uuid+",ERROR,class=com.mobios.vaseline.controllers.MainController,method=isToMsisdnAvailable,exception="+e);
		}finally{
			if ( conn != null ){
				try{ 
					conn.close(); 
				}catch(Exception ce){
					//LogUtil.getErrorLog().debug(uuid+",ERROR,class=com.mobios.vaseline.controllers.MainController,method=isToMsisdnAvailable,conn close,exception="+ce);
					//LogUtil.getEventLog().debug(uuid+",ERROR,class=com.mobios.vaseline.controllers.MainController,method=isToMsisdnAvailable,conn close,exception="+ce);
				}
			}
		}
		
		return json;
		
	}
	
	
	/**
	 * retrieve data by minutes in a hour
	 * @param query
	 * @return
	 */
	public String getDataByMinute(String query, String hour){
		JsonObject jsonObject = new JsonObject();
		
		Connection conn = null;
		Statement st = null;
		String json = null;


		try {	
			int hourr = Integer.parseInt(hour);
			
			if(hourr == 0){
				hourr = 12;
			}else if (hourr > 12){
				hourr = hourr - 12;
			}else {
				hourr = hourr;
			}
			
			hour = String.valueOf(hourr);
			
			String minuteArr[] = {"00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29",
					"30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59"};
			
				
			
			System.out.println("query "+query);
			for(int i=0; i<minuteArr.length; i++){
				
				jsonObject.addProperty(String.valueOf(hourr)+"."+minuteArr[i], "0");
			}
			
			System.out.println("arr : "+jsonObject.toString());
			
			conn = DatabaseController.getInstance().getMysqlConnection();
			if (conn == null) {
				
				return null;
			}
			
			ResultSet rs = null;
			st = (Statement) conn.createStatement();
			rs = st.executeQuery(query);
			
			while (rs.next()) {
				String s=""+rs.getString("t_minute");
				s=s.replace(":", ".");
				
				if(s.startsWith("0")){
					s=s.substring(1,s.length());
				}
				jsonObject.addProperty(s, rs.getString("count"));
				System.out.println("t minute :"+rs.getString("t_minute"));
			}		
			
			json = jsonObject.toString();
			System.out.println("json : "+json);
			
			rs.close();
			st.close();
			
		}catch(Exception e){
			System.out.println(e);
			//LogUtil.getErrorLog().debug(uuid+",ERROR,class=com.mobios.vaseline.controllers.MainController,method=isToMsisdnAvailable,exception="+e);
			//LogUtil.getEventLog().debug(uuid+",ERROR,class=com.mobios.vaseline.controllers.MainController,method=isToMsisdnAvailable,exception="+e);
		}finally{
			if ( conn != null ){
				try{ 
					conn.close(); 
				}catch(Exception ce){
					//LogUtil.getErrorLog().debug(uuid+",ERROR,class=com.mobios.vaseline.controllers.MainController,method=isToMsisdnAvailable,conn close,exception="+ce);
					//LogUtil.getEventLog().debug(uuid+",ERROR,class=com.mobios.vaseline.controllers.MainController,method=isToMsisdnAvailable,conn close,exception="+ce);
				}
			}
		}
		
		return json;
		
	}

}