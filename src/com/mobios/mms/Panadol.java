package com.mobios.mms;

public class Panadol {
  private String date_time;
  
  public Panadol() {}
  
  public String getDate_time() { return date_time; }
  
  public void setDate_time(String date_time) {
    this.date_time = date_time;
  }
  
  public String getMsisdn() { return msisdn; }
  
  public void setMsisdn(String msisdn) {
    this.msisdn = msisdn;
  }
  
  public int getOption2() { return option2; }
  
  public void setOption2(int option2) {
    this.option2 = option2;
  }
  
  private String msisdn;
  private int option2;
}
