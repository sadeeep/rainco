package com.mobios.mms;

public class knorMF { private String date_time;
  private String naOnnet;
  private String naoffnet;
  private String thOnnet;
  private String thoffnet;
  private String info1;
  private String info2;
  
  public knorMF() {}
  
  public String getDate_time() { return date_time; }
  
  public void setDate_time(String date_time) {
    this.date_time = date_time;
  }
  
  public String getNaOnnet() { return naOnnet; }
  
  public void setNaOnnet(String naOnnet) {
    this.naOnnet = naOnnet;
  }
  
  public String getNaoffnet() { return naoffnet; }
  
  public void setNaoffnet(String naoffnet) {
    this.naoffnet = naoffnet;
  }
  
  public String getThOnnet() { return thOnnet; }
  
  public void setThOnnet(String thOnnet) {
    this.thOnnet = thOnnet;
  }
  
  public String getThoffnet() { return thoffnet; }
  
  public void setThoffnet(String thoffnet) {
    this.thoffnet = thoffnet;
  }
  
  public String getInfo1() { return info1; }
  
  public void setInfo1(String info1) {
    this.info1 = info1;
  }
  
  public String getInfo2() { return info2; }
  
  public void setInfo2(String info2) {
    this.info2 = info2;
  }
}
