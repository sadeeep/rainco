package com.mobios.mms;

public class knorrMFDB { private String date_time;
  private String status;
  private String count;
  private String msg_type;
  private String info1;
  
  public knorrMFDB() {}
  
  public String getDate_time() { return date_time; }
  
  public void setDate_time(String date_time) {
    this.date_time = date_time;
  }
  
  public String getStatus() { return status; }
  
  public void setStatus(String status) {
    this.status = status;
  }
  
  public String getCount() { return count; }
  
  public void setCount(String count) {
    this.count = count;
  }
  
  public String getMsg_type() { return msg_type; }
  
  public void setMsg_type(String msg_type) {
    this.msg_type = msg_type;
  }
  
  public String getInfo1() { return info1; }
  
  public void setInfo1(String info1) {
    this.info1 = info1;
  }
}
