<%@page import="java.sql.ResultSet"%>
<%@page import="com.mobios.model.datefilter"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.google.gson.JsonObject"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="com.mobios.mms.MainController"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>


<% 

MainController mc = MainController.getInstance();

String action = request.getParameter("action");
//String date = "2015-03-01";

//get data hourly in a day
if(action.equals("get_json")){
	String minSlider = ""+request.getParameter("min");
	String maxSlider = ""+request.getParameter("max");
	String date = ""+request.getParameter("date");
	String cp_id = ""+request.getParameter("cp_id");
	
	//System.out.println("slider min : "+minSlider+" slider max : "+maxSlider);
	//String qryMisdCalls = "select hour(date_time)+1 as t_hour, count(hour(date_time)) as count from missed_calls where date(date_time) = '"+date+"' and hour(date_time) between '"+minSlider+"' and '"+maxSlider+"' group by t_hour ;";
	//String qryObdEngs = "select hour(date_time)+1 as t_hour, count(hour(date_time)) as count from obd_engagements where date(date_time) = '"+date+"' and hour(date_time) between '"+minSlider+"' and '"+maxSlider+"' group by t_hour ;";
	//String qrySmsEngs = "select hour(date_time)+1 as t_hour, count(hour(date_time)) as count from sms_engagements where date(date_time) = '"+date+"' and hour(date_time) between '"+minSlider+"' and '"+maxSlider+"' group by t_hour ;";
	String qryMisdCalls = "select hour(date_time)+1 as t_hour, count(hour(date_time)) as count from rainco_users where date(date_time) = '"+date+"' group by t_hour";
	String qryObdEngs = "select hour(date_time)+1 as t_hour, count(hour(date_time)) as count from obd_engagements where date(date_time) = '"+date+"' AND cp_id='"+cp_id+"' group by t_hour";
	String qrySmsEngs = "select hour(date_time)+1 as t_hour, count(hour(date_time)) as count from rainco_users where date(date_time) = '"+date+"' AND status = 'REDEEMED' AND secret_code IS NOT NULL group by t_hour";
	
	
	String misdCalls = mc.getData(qryMisdCalls, minSlider, maxSlider);
	String obdEngs = mc.getData(qryObdEngs, minSlider, maxSlider);
	String smsEngs = mc.getData(qrySmsEngs, minSlider, maxSlider);
	
	JsonObject jsObj = new JsonObject();
	jsObj.addProperty("misd", misdCalls);
	jsObj.addProperty("obd", obdEngs);
	jsObj.addProperty("sms", smsEngs);
	
	String jsonArr = jsObj.toString();
	
	response.getWriter().print(jsonArr);
	//out.print(data);
	
}

if(action.equals("get_json_2")){
	String minSlider = ""+request.getParameter("min");
	String maxSlider = ""+request.getParameter("max");
	String date = ""+request.getParameter("date");
	String cp_id = ""+request.getParameter("cp_id");
	
	//System.out.println("slider min : "+minSlider+" slider max : "+maxSlider);
	//String qryMisdCalls = "select hour(date_time)+1 as t_hour, count(hour(date_time)) as count from missed_calls where date(date_time) = '"+date+"' and hour(date_time) between '"+minSlider+"' and '"+maxSlider+"' group by t_hour ;";
	//String qryObdEngs = "select hour(date_time)+1 as t_hour, count(hour(date_time)) as count from obd_engagements where date(date_time) = '"+date+"' and hour(date_time) between '"+minSlider+"' and '"+maxSlider+"' group by t_hour ;";
	//String qrySmsEngs = "select hour(date_time)+1 as t_hour, count(hour(date_time)) as count from sms_engagements where date(date_time) = '"+date+"' and hour(date_time) between '"+minSlider+"' and '"+maxSlider+"' group by t_hour ;";
	String qryMisdCalls = "select hour(date_time)+1 as t_hour, count(hour(date_time)) as count from missed_calls where serv='94720760160' AND date(date_time) = '"+date+"' AND cp_id='"+cp_id+"'  group by t_hour;";
	String qryObdEngs = "select hour(date_time)+1 as t_hour, count(hour(date_time)) as count from missed_calls where serv='94720760161' AND date(date_time) = '"+date+"' AND cp_id='"+cp_id+"' group by t_hour;";
	String qrySmsEngs = "select hour(date_time)+1 as t_hour, count(hour(date_time)) as count from obd_engagements where date(date_time) = '"+date+"' AND cp_id='"+cp_id+"' group by t_hour;";
	
	
	String misdCalls = mc.getData(qryMisdCalls, minSlider, maxSlider);
	String obdEngs = mc.getData(qryObdEngs, minSlider, maxSlider);
	String smsEngs = mc.getData(qrySmsEngs, minSlider, maxSlider);
	
	JsonObject jsObj = new JsonObject();
	jsObj.addProperty("misd", misdCalls);
	jsObj.addProperty("obd", obdEngs);
	jsObj.addProperty("sms", smsEngs);
	
	String jsonArr = jsObj.toString();
	
	response.getWriter().print(jsonArr);
}
if(action.equals("get_mints_2")){
	//String minSlider = ""+request.getParameter("min");
	//String maxSlider = ""+request.getParameter("max");
	String date = ""+request.getParameter("date");
	String hour = ""+request.getParameter("hour");
	String cp_id = ""+request.getParameter("cp_id");
	//System.out.println("hour : "+hour);
	
	String qryMisdCalls = "select DATE_FORMAT(date_time, '%h:%i') as t_minute, count(minute(date_time)) as count from missed_calls where serv='94720760160' AND date(date_time) = '"+date+"' and hour(date_time) = '"+hour+"' AND cp_id='"+cp_id+"' group by t_minute; ";
	String qryObdEngs = "select DATE_FORMAT(date_time, '%h:%i') as t_minute, count(minute(date_time)) as count from missed_calls where serv='94720760161' AND date(date_time) = '"+date+"' and hour(date_time) = '"+hour+"' AND cp_id='"+cp_id+"' group by t_minute; ";
	String qrySmsEngs = "select DATE_FORMAT(date_time, '%h:%i') as t_minute, count(minute(date_time)) as count from obd_engagements where date(date_time) = '"+date+"' and hour(date_time) = '"+hour+"' AND cp_id='"+cp_id+"' group by t_minute; ";
		
	String misdCalls = mc.getDataByMinute(qryMisdCalls, hour);
	String obdEngs = mc.getDataByMinute(qryObdEngs, hour);
	String smsEngs = mc.getDataByMinute(qrySmsEngs, hour);
	
	JsonObject jsObj = new JsonObject();
	jsObj.addProperty("misd", misdCalls);//stjosephs
	jsObj.addProperty("obd", obdEngs);//isipathana
	jsObj.addProperty("sms", smsEngs);
	
	String jsonArr = jsObj.toString();
	//System.out.println("jsonArr : "+jsonArr);
	
	response.getWriter().print(jsonArr);
	//out.print(data);	
}


//get data by minutes in a hour
if(action.equals("get_mints")){
	//String minSlider = ""+request.getParameter("min");
	//String maxSlider = ""+request.getParameter("max");
	String date = ""+request.getParameter("date");
	String hour = ""+request.getParameter("hour");
	String cp_id = ""+request.getParameter("cp_id");
	//System.out.println("hour : "+hour);
	
	//System.out.println("slider min : "+minSlider+" slider max : "+maxSlider);
	//String qryMisdCalls = "select minute(date_time) as t_minute, count(minute(date_time)) as count from missed_calls where date(date_time) = '"+date+"' and hour(date_time) = '"+hour+"' group by t_minute; ";
	//String qryObdEngs = "select minute(date_time) as t_minute, count(minute(date_time)) as count from obd_engagements where date(date_time) = '"+date+"' and hour(date_time) = '"+hour+"' group by t_minute; ";
	//String qrySmsEngs = "select minute(date_time) as t_minute, count(minute(date_time)) as count from sms_engagements where date(date_time) = '"+date+"' and hour(date_time) = '"+hour+"' group by t_minute; ";
	String qryMisdCalls = "select DATE_FORMAT(date_time, '%h:%i') as t_minute, count(minute(date_time)) as count from missed_calls where date(date_time) = '"+date+"' and hour(date_time) = '"+hour+"' AND cp_id='"+cp_id+"' group by t_minute";
	String qryObdEngs = "select DATE_FORMAT(date_time, '%h:%i') as t_minute, count(minute(date_time)) as count from obd_engagements where date(date_time) = '"+date+"' and hour(date_time) = '"+hour+"' AND cp_id='"+cp_id+"' group by t_minute";
	String qrySmsEngs = "select DATE_FORMAT(date_time, '%h:%i') as t_minute, count(minute(date_time)) as count from sms_engagements where date(date_time) = '"+date+"' and hour(date_time) = '"+hour+"' AND cp_id='"+cp_id+"' group by t_minute";
		
	String misdCalls = mc.getDataByMinute(qryMisdCalls, hour);
	String obdEngs = mc.getDataByMinute(qryObdEngs, hour);
	String smsEngs = mc.getDataByMinute(qrySmsEngs, hour);
	
	JsonObject jsObj = new JsonObject();
	jsObj.addProperty("misd", misdCalls);
	jsObj.addProperty("obd", obdEngs);
	jsObj.addProperty("sms", smsEngs);
	
	String jsonArr = jsObj.toString();
	//System.out.println("jsonArr : "+jsonArr);
	
	response.getWriter().print(jsonArr);
	//out.print(data);	
}

 if(action.equals("getCategoryDate_Server")){//action for search by datewise and server

	String buffer1="";
	String short_code =String.valueOf(request.getParameter("short"));
	String datetype2 =String.valueOf(request.getParameter("dtype2"));
	String datetype =String.valueOf(request.getParameter("dtype"));
	String servertype =""+request.getParameter("server");
	System.out.println("short_code:"+short_code+""+"datetype2:"+datetype2+""+"datetype:"+datetype+""+"servertype:"+servertype);
	
	if (datetype2.length()==0||datetype.length()==0 || servertype.length()==0 ||(datetype.length()==0 & servertype.length()==0)||datetype.equalsIgnoreCase("Start Date")||datetype2.equalsIgnoreCase("End Date")) {
	
	//	LogUtil.getLog("Event").debug(logData+",Searchdatewise_server,serverstatus:null");		
		response.getWriter().println(buffer1);
		

	}
	
	else  {
		
		List<datefilter> result1= new ArrayList<datefilter>();
		result1=mc.summery_byDateandServer(datetype, short_code,datetype2 );
		if(result1.contains("")|| result1.size()==0||result1==null){
			 buffer1="none";
			 response.getWriter().println(buffer1);
		//	 LogUtil.getLog("Event").debug(logData+",Searchdatewise,serverstatus:nodata");		 
		 }
		 else{
			for(int i=0;i<result1.size();i++)   {
				buffer1+=  result1.get(i).getDatewise_date()+","+result1.get(i).getDatewise_etisalatnum()+","+result1.get(i).getDatewise_nonetisalatnum()+",";
		//buffer1+=  result1.get(i).getDate1()+","+result1.get(i).getAr_row()[0]+","+result1.get(i).getAr_row()[1]+",";
		//	LogUtil.getLog("Event").debug(logData+",Searchdatewise,serverstatus:sucess,Fromdate:"+datetype+",Todate:"+datetype2+",server_no:"+servertype+"");		
	
		
		}
			String file=mc.excel_bydate(result1);
			buffer1+=file;
			response.getWriter().println(buffer1);	
	}}
}
 if(action.equals("getCategoryDuration_Server")){//action for search by durationwise and server
	
	String buffer1="";
	String short_code =String.valueOf(request.getParameter("short"));
	String datetype2 =String.valueOf(request.getParameter("dtype2"));
	String datetype =String.valueOf(request.getParameter("dtype"));
	String servertype =""+request.getParameter("server").trim();
	System.out.println("short_code:"+short_code+""+"datetype2:"+datetype2+""+"datetype:"+datetype+""+"servertype:"+servertype);
	if (datetype2.length()==0||datetype.length()==0 || servertype.length()==0 ||(datetype.length()==0 & servertype.length()==0)||datetype.equalsIgnoreCase("Start Date")||datetype2.equalsIgnoreCase("End Date")) {
	
		//LogUtil.getLog("Event").debug(logData+",Searchdurationwise_server,serverstatus:null");		
		response.getWriter().println(buffer1);
		

	}
	else  {
	//	MainController mc=new MainController();
		List<datefilter> result1= new ArrayList<datefilter>();
		 result1=mc.summery_byDateandDuration(datetype, short_code,datetype2 );
		 if(result1.contains("")|| result1.size()==0||result1==null){
			 buffer1="none";
			 response.getWriter().println(buffer1);
			// LogUtil.getLog("Event").debug(logData+",Searchbyduration_server,serverstatus:nodata");		 
		 }
		 else{
			for(int i=0;i<result1.size();i++)   {
	
		buffer1+=  result1.get(i).getDuration()+","+result1.get(i).getDurationwise_etisalatnum()+","+result1.get(i).getDurationwise_nonetisalatnum()+",";
		
	//	LogUtil.getLog("Event").debug(logData+",Searchdurationwise,serverstatus:sucess,Fromdate:"+datetype+",Todate:"+datetype2+",server_no:"+servertype+"");		
		
		}
			String file=mc.excel_byduration(result1);
			buffer1+=file;
			System.out.println("response"+buffer1);
			response.getWriter().println(buffer1);	
	}}
}
if(action.equals("getMisscallsSummery")){//action for search by durationwise and server
	
	String buffer1="";
	String short_code =String.valueOf(request.getParameter("short"));
	String datetype2 =String.valueOf(request.getParameter("dtype2"));
	String datetype =String.valueOf(request.getParameter("dtype"));
	String servertype =""+request.getParameter("server").trim();
	System.out.println("short_code:"+short_code+""+"datetype2:"+datetype2+""+"datetype:"+datetype+""+"servertype:"+servertype);
	if (datetype2.length()==0||datetype.length()==0 || servertype.length()==0 ||(datetype.length()==0 & servertype.length()==0)||datetype.equalsIgnoreCase("Start Date")||datetype2.equalsIgnoreCase("End Date")) {
	
	//	LogUtil.getLog("Event").debug(logData+",Searchmisscallsummery,serverstatus:null");		
		response.getWriter().println(buffer1);
		

	}
	else  {
	//	MainController mc=new MainController();
		List<datefilter> result1= new ArrayList<datefilter>();
		 result1=mc.summery_misscalls(datetype, short_code,datetype2 );
		 
		 if(result1.contains("")|| result1.size()==0||result1==null){
			 buffer1="none";
			 response.getWriter().println(buffer1);
		//	 LogUtil.getLog("Event").debug(logData+",Searchmissedcalls summery,serverstatus:nodata");		 
		 }
		 else{
			for(int i=0;i<result1.size();i++)   {
	
		buffer1+=  result1.get(i).getMisscall_date()+","+result1.get(i).getSum_misscall()+",";
		
//		LogUtil.getLog("Event").debug(logData+",Searchmisscallsummery,serverstatus:sucess,Fromdate:"+datetype+",Todate:"+datetype2+",server_no:"+servertype+"");		
		
		}
			String file=mc.excel_missedcall(result1);
			buffer1+=file;
			
			response.getWriter().println(buffer1);	
			
	}
}
}
if(action.equals("get_FromDate_ToDate")){//action for search from to date
	
	String campaign =String.valueOf( request.getParameter("campaign"));
	 System.out.println("lll*****2***"+campaign);
		String buffer="";
	
	//	MainController mc=new MainController();
      ResultSet rs=mc.get_from_to(campaign);
			
		 System.out.println("*****2***"+rs.getString("start_date"));
			buffer= rs.getString("start_date")+","+rs.getString("end_date");
			response.getWriter().println(buffer);	
	
		
}





%>