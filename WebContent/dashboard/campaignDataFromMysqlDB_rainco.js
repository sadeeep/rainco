/**
 * Created by Nadee on 03/01/17.
 */

$(document).ready(function () {
    
	 var today = new Date();
     var dd = today.getDate();
     var mm = today.getMonth()+1; //January is 0!

     var yyyy = today.getFullYear();
     if(dd<10){
         dd='0'+dd;
     }
     if(mm<10){
         mm='0'+mm;
     }
     var today = yyyy+'-'+mm+'-'+dd;
    
  // alert("test");
     var jqxhr =
    	    $.ajax({
    	        url: "DashboardController_rainco.jsp?action=getmissedCallSummery",
    	        data: {
    	            name : "The name",
    	            desc : "The description"
    	        }
    	    })
    	    .done  (function(response, textStatus, jqXHR)  
    	    		{ 
    	    			//alert("Success: " + response.trim()) ;
    	    			obj = JSON.parse(response.trim());
    	    			//alert("response"+obj.countList[0]);
    	    			 $("#campaign-missedcall-summery").append("<h4 class='sub'><span>Campaign Results Summary</span>" +
    	    	                    "</h4>" +
    	    	                    "<ul class='stats-container'>" +
	    	    	                    "<li><a href='RaincoNoList.jsp?operator=OTP' class='stat summary'>" +
	    	    	                    "<span class='icon icon-circle bg-red'>" +
	    	    	                    "<i class='icon-stats'></i>" +
	    	    	                    "</span><span class='digit' id='tot-missed'>" +
	    	    	                    "<span class='text'>Calls for OTP</span>" +obj.countList[0]+
	    	    	                    "</span>" +
	    	    	                    "</a></li>" +
	    	    	                    "<li><a href='RaincoNoList.jsp?operator=SCT' class='stat summary'>" +
	    	    	                    "<span class='icon icon-circle bg-green'>" +
	    	    	                    "<i class='icon-stats'></i>" +
	    	    	                    "</span><span class='digit' id='tot-obd-engagement'>" +
	    	    	                    "<span class='text'>Calls for Secret Code</span>" +obj.countList[1]+
	    	    	                    "</span>" +
	    	    	                    "</a></li>" +
	    	    	                    "<li><a href='RaincoNoList.jsp?operator=BLACK' class='stat summary'>" +
	    	    	                    "<span class='icon icon-circle bg-blue'>" +
	    	    	                    "<i class='icon-stats'></i>" +
	    	    	                    "</span><span class='digit' id='tot-sms-engagement'>" +
	    	    	                    "<span class='text'>Blacklisted Calls</span>" +obj.countList[2]+
	    	    	                    "</span>" +
	    	    	                    "</a></li>" +
	    	    	                    "<li><a href='RaincoNoList.jsp?operator=REDEEMED' class='stat summary'>" +
	    	    	                    "<span class='icon icon-circle bg-orange'>" +
	    	    	                    "<i class='icon-stats'></i>" +
	    	    	                    "</span><span class='digit' id='tot-sms-engagement'>" +
	    	    	                    "<span class='text'>Redeemed Coupons</span>" +obj.countList[3]+
	    	    	                    "</span>" +
	    	    	                    "</a></li>" +
    	    	                    "</ul>");

    	    	                   
    	    	})
    	    .fail  (function(jqXHR, textStatus, errorThrown)
    	    		{
    	    			alert("Error"+jqXHR + "*textStatus :"+textStatus+ "*errorThrown:"+errorThrown)   ;
    	    		});
     
    
     
     
    // alert("today"+today);
     $.ajax({
    	 //url: 'DashboardController_maggi.jsp?action=getMissCallSummeryByDate&date_time='+today,
         url: 'DashboardController_rainco.jsp?action=getMissCallSummeryByDate&date_time='+today,
         contentType: 'application/json',
         context: document.body
     }).done(function (response) {
    	 //alert(response);
         obj = JSON.parse(response.trim());
			//alert("response count :"+obj.countListByDate[0]);

            
             $("#summeryByDate").append("<div class='' style=''><ul class='stats-container'>" +
                 "<li><span id='total-summery-voice' class='btn btn-info' style='width: 180px;'>Calls for OTP: <br><font size='2' style='font-weight: 900' color='#000000'>" + obj.countListByDate[0]+"</font></span></li>" +
                 "<li><span id='total-summery-obd' class='btn btn-warning' style='width: 180px;'>Calls for Secret Code: <br><font size='2' style='font-weight: 900' color='#000000'>" + obj.countListByDate[1]+"</font></span></li>" +
                 "<li><span id='total-summery-sms' class='btn btn-success' style='width: 180px;'>Blacklisted Calls: <br><font size='2' style='font-weight: 900' color='#000000'>" + obj.countListByDate[2]+"</font></span></li>" +
                 "<li><span id='total-summery-sms2' class='btn btn-warning' style='width: 180px;'>Redeemed Coupons: <br><font size='2' style='font-weight: 900' color='#000000'>" + obj.countListByDate[3]+"</font></span></li>" +
                 "</ul></div>");
         });
     
     
     
     
     var jqxhr =
  	    $.ajax({
  	        url: "DashboardController_rainco.jsp?action=getOperatorWiseReloadAmount",
  	        data: {
  	            name : "The name",
  	            desc : "The description"
  	        }
  	    })
  	    .done  (function(response, textStatus, jqXHR)  
  	    		{ //<a href='RaincoNoList.jsp?operator=MOBITEL' class='stat summary' rel='tooltip' title='50 : "+obj.countListByDate[5]+" / 100 : "+obj.countListByDate[6]+"'>
  	    			//alert("Success: " + response.trim()) ;
  	    			obj = JSON.parse(response.trim());
  	    			//alert("object "+obj);
  	    			//alert("response"+obj.countListByDate[0]);
  	    			//<div class='span4' rel='popover' data-trigger='hover' title='Euphoria' data-placement='top' data-content='Test'></div>
  	    			
  	    			 $("#campaign-missedcall-summery2").append("<h4 class='sub'><span>Operator Wise Reload Amount</span>" +
  	    				 "</h4>" +
  	    	                    "<ul class='stats-container'>" +  	    	                    
 	    	    	                    "<li><a href='RaincoNoList.jsp?operator=MOBITEL' class='stat summary' rel='tooltip' title='Reload Type | Reload Count | Reload Total&#013;        50                     "+obj.countListByDate[5]+"                   "+obj.countListByDate[15]+" &#013;"+"        100                     "+obj.countListByDate[6]+"                   "+obj.countListByDate[16]+"'>"+
  	    	                    		"<span class='icon '>" +	    	    	                    
 	    	    	                    "<img src='assets/images/mobitel_logo.png' >"+ 	    	                     	    	    	                   
 	    	    	                    "</span>" +
 	    	    	                    "<span class='digit' id='tot-missed'>" +	                                                                                   
	                                    "<span class='text'>Mobitel </span>" +obj.countListByDate[0]+
 	    	    	                    "</span>" +
 	    	    	                    "</a></div></li>" +	    	    	                    
 	    	    	                    "<li><a href='RaincoNoList.jsp?operator=DIALOG' class='stat summary' rel='tooltip' title='Reload Type | Reload Count | Reload Total&#013;        50                     "+obj.countListByDate[7]+"                 "+obj.countListByDate[17]+" &#013;"+"        100                   "+obj.countListByDate[8]+"                 "+obj.countListByDate[18]+"'>"+
 	    	    	                    "<span class='icon '>" +	    	    	                    
	    	    	                    "<img src='assets/images/dialog_logo.png' > "+ 	    	                     	    	    	                   
	    	    	                    "</span>" +
 	    	    	                    "<span class='digit' id='tot-obd-engagement'>" +
 	    	    	                    "<span class='text'>Dialog </span>" +obj.countListByDate[1]+
 	    	    	                    "</span>" +
 	    	    	                    "</li>" + 	    	    	                    
 	    	    	                    "<li><a href='RaincoNoList.jsp?operator=ETISELAT' class='stat summary' rel='tooltip' title='Reload Type | Reload Count | Reload Total&#013;        50                     "+obj.countListByDate[9]+"                   "+obj.countListByDate[19]+" &#013;"+"        100                   "+obj.countListByDate[10]+"                   "+obj.countListByDate[20]+"'>"+
 	    	    	                    "<span class='icon '>" +	    	    	                    
	    	    	                    "<img src='assets/images/Etisalat_logo2.png' > "+ 	    	                     	    	    	                   
	    	    	                    "</span>" +
 	    	    	                    "<span class='digit' id='tot-sms-engagement'>" +
 	    	    	                    "<span class='text'>Etiselat</span>" +obj.countListByDate[2]+
 	    	    	                    "</span>" +
 	    	    	                    "</a></li>" +
 	    	    	                    "<li><a href='RaincoNoList.jsp?operator=HUTCH' class='stat summary' rel='tooltip' title='Reload Type | Reload Count | Reload Total&#013;        50                     "+obj.countListByDate[11]+"                   "+obj.countListByDate[21]+" &#013;"+"        100                   "+obj.countListByDate[12]+"                   "+obj.countListByDate[22]+"'>"+
 	    	    	                    "<span class='icon '>" +	    	    	                    
	    	    	                    "<img src='assets/images/hutch_logo.png' > "+ 	    	                     	    	    	                   
	    	    	                    "</span>" +
	    	    	                    "<span class='digit' id='tot-sms-engagement'>" +
	    	    	                    "<span class='text'>Hutch  </span>" +obj.countListByDate[3]+
	    	    	                    "</span>" +
	    	    	                    "</a></li>" +
	    	    	                    "<li><a href='RaincoNoList.jsp?operator=AIRTEL' class='stat summary' rel='tooltip' title='Reload Type | Reload Count | Reload Total&#013;        50                     "+obj.countListByDate[13]+"                   "+obj.countListByDate[23]+" &#013;"+"        100                   "+obj.countListByDate[14]+"                   "+obj.countListByDate[24]+"'>"+
	    	    	                    "<span class='icon '>" +	    	    	                    
	    	    	                    "<img src='assets/images/airtel_logo.png' > "+ 	    	                     	    	    	                   
	    	    	                    "</span>" +
 	    	    	                    "<span class='digit' id='tot-sms-engagement'>" +
 	    	    	                    "<span class='text'>Airtel </span>" +obj.countListByDate[4]+
 	    	    	                    "</span>" +
 	    	    	                    "</a></li>" +
  	    	                    "</ul>");
              
  	    	})
  	    .fail  (function(jqXHR, textStatus, errorThrown)
  	    		{
  	    			alert("Error"+jqXHR + "*textStatus :"+textStatus+ "*errorThrown:"+errorThrown)   ;
  	    		});
     
    
});


 //getSummeryDataBySelectedDate() summery-data-date-from
 function getSummeryDataBySelectedDate(){
	    var selected_date_time=$("#summery-data-date-from").val();
	   // var numberListByDate=$("#numberListByDate").val();
	    
	    if(selected_date_time=="" || selected_date_time==null){
	        alert("Please select the date.");
	        return;
	    }else{

	    $.ajax({
	        url: 'DashboardController_rainco.jsp?action=getMissCallSummeryByDate&date_time='+selected_date_time,
	        contentType: 'application/json',
	        context: document.body
	    }).done(function (response) {
	           obj = JSON.parse(response.trim());
	            document.getElementById("total-summery-voice").innerHTML="Calls for OTP: <br><font size='2' style='font-weight: 900' color='#000000'>"+obj.countListByDate[0]+"</font>";
	            document.getElementById("total-summery-obd").innerHTML="Calls for Secret Code:<br><font size='2' style='font-weight: 900' color='#000000'>"+obj.countListByDate[1]+"</font>";
	            document.getElementById("total-summery-sms").innerHTML="Blacklisted Calls: <br><font size='2' style='font-weight: 900' color='#000000'>"+obj.countListByDate[2]+"</font>";
	            document.getElementById("total-summery-sms2").innerHTML="Redeemed Coupons: <br><font size='2' style='font-weight: 900' color='#000000'>"+obj.countListByDate[3]+"</font>";
	           // document.getElementById("total-summery-max-try").innerHTML="Recorded Messages:<br><font size='2' style='font-weight: 900' color='#000000'>"+obj.countListByDate[3]+"</font>";
	        });
	    }
	
	}
 