<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en">
<!--<![endif]-->

<head>
<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Bootstrap Stylesheet -->
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css"
	media="all">

<!-- jquery-ui Stylesheets -->
<link rel="stylesheet" href="assets/jui/css/jquery-ui.css"
	media="screen">
<link rel="stylesheet" href="assets/jui/jquery-ui.custom.css"
	media="screen">
<link rel="stylesheet"
	href="assets/jui/timepicker/jquery-ui-timepicker.css" media="screen">

<!-- Uniform Stylesheet -->
<link rel="stylesheet" href="plugins/uniform/css/uniform.default.css"
	media="screen">

<!-- Plugin Stylsheets first to ease overrides -->

<!-- End Plugin Stylesheets -->

<!-- Main Layout Stylesheet -->
<link rel="stylesheet" href="assets/css/fonts/icomoon/style.css"
	media="screen">
<link rel="stylesheet" href="assets/css/main-style.css" media="screen">

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<title>MMS</title>

</head>

<body data-show-sidebar-toggle-button="false" data-fixed-sidebar="false" onload="getSummeryDataBySelectedDate()">
	<%
		Date dt = new Date();
		SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
	%>
	<div id="wrapper">
		<header id="header" class="navbar navbar-inverse">
			<div class="navbar-inner">
				<div class="container">
					<div class="brand-wrap pull-left">
						<div class="brand-img">
							<a class="brand" href="#"> <img
								src="assets/images/mobios_logo.png" alt=""
								style="width: 193px; height: 75px;">
							</a>
						</div>
					</div>
				
					<div id="header-right" class="clearfix">
						<div id="nav-toggle" data-toggle="collapse"
							data-target="#navigation" class="collapsed">
							<i class="icon-caret-down"></i>
						</div>


						<div id="header-functions" class="pull-right">
							<div id="user-info" class="clearfix">
								<span class="info"> Welcome <span class="name">Admin</span>
								</span>
								<div class="avatar">
									<a class="dropdown-toggle" href="#" data-toggle="dropdown">
										<img src="assets/images/defaltUser.png" alt="Avatar">
									</a>
									<ul class="dropdown-menu pull-right">
										<li><a href="#"><i class="icos-refresh-3"></i>Reset
												Password</a></li>

										<li class="divider"></li>
										<li><a href="index.jsp"><i class="icol-key"></i>
												Logout</a></li>
									</ul>
								</div>
							</div>
							<div id="logout-ribbon">
								<a href="Login.jsp"><i class="icon-off"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>

		<div id="content-wrap">
			<div id="content" class="sidebar-minimized">
				<div id="content-outer">
					<div id="content-inner">
						<aside id="sidebar">
							<nav id="navigation" class="collapse">
								<ul>
									<li class="active"><a href="DashboardRainco.jsp"> <span
											title="Craete Dealer"> <i class="icon-pie-chart-2"></i>
												<span class="nav-title">Dashboard</span>
										</span>
									</a></li>
									<li><a href="RaincoBlock.jsp"> <span
											title="Craete Dealer"> <i class="icon-ban-circle"></i>
												<span class="nav-title">Black Listed</span>
										</span>
									</a></li>
									<li><a href="RaincoNoList.jsp"> <span
											title="Craete Dealer"> <i class="icon-gift"></i> <span
												class="nav-title">Redeemed Coupons</span>
										</span>
									</a></li>
									<!-- <li><a href="ReloadList.jsp"> <span
											title="Craete Dealer"> <i class="icon-gift"></i> <span
												class="nav-title">Reloads by Operator</span>
										</span>
									</a></li> -->
	
								</ul>
							</nav>
						</aside>

						<div id="sidebar-separator"></div>

						<section id="main" class="clearfix">
							<div id="main-header" class="page-header">
								<ul class="breadcrumb">
									<li><i class="icon-home"></i>General <span class="divider">&raquo;</span>
									</li>
									<li><a href="#">Dashboard</a> <span class="divider">&raquo;</span>
									</li>
								</ul>

								<h1 id="main-heading">Charts Gallery</h1>
							</div>
						
							<div id="main-content">
								
								<div class="row-fluid">
									<div id="campaign-missedcall-summery">

										<div class="controls"></div>
									</div>
									<div id="campaign-summery-list"></div>
								</div>

								<h4 class='sub'>
									<span>Campaign Results Search By Date</span>
								</h4>
								
								<div class="navbar-inner" id="summeryByDate">

									<div class="control-group" style="text-align: center;">
									
										<label class="control-label" style="padding-top: 12px;"><b
											style="padding-right: 167px">Select a Date</b></label>
											
										<div class="controls" id="missedcall-summury-by-date">
											<input id="summery-data-date-from"
												class="input-medium ndshaatepicker-basic" type="text"
												value="<%=ft.format(dt)%>">
												<input id="summery-data-date-from"
												class="input-medium ndshaatepicker-basic" type="text"
												value="<%=ft.format(dt)%>">
											<button type="submit" class="btn btn-primary"
												onclick="getSummeryDataBySelectedDate()"
												style="position: relative; top: -5px;">Search</button>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</div>										
									</div>
								</div>
								
								
								<div class="row-fluid">
									<div id="campaign-missedcall-summery2">

										<div class="controls"></div>
									</div>
									<div id="campaign-summery-list"></div>
								</div>

								 <div class="row-fluid">
									<div class="span6 widget">
										<div class="widget-header">
											<span class="title"><i class="icon-graph"></i>Missed
												calls summery</span>
										</div>
										<div class="widget-content">
											<div id="demo-charts-01" style="height: 300px;"></div>
										</div>
									</div>
									<div class="span6 widget">
										<div class="widget-header">
											<span class="title"><i class="icon-pie-chart"></i>Total Reloads by Operator Wise</span>
										</div>
										<div class="widget-content">
											<div id="demo-charts-05" style="height: 300px;"></div>
										</div>
									</div>
								</div>
								
								
								 <div class="row-fluid">
									<div id="dashboard-demo"
										class="tabbable analytics-tab paper-stack">
										<ul class="nav nav-tabs">
											<li><a href="#" data-target="#math" data-toggle="tab"><i
													class="icon-graph"></i> Daily Report</a></li>

										</ul>
										<div class="tab-content">

											<div id="math" class="tab-pane">
												<div class="analytics-tab-header clearfix">
													<div class="pull-left" style="width: 100%">
														<div class="control-group">

															<label class="control-label">Range of (Hours)</label>
															<div class="controls">
																<div id="math-x-range"></div>
															</div>
															<div class="row-fluid">
																<div class="span6 ">
																	<label class="control-label">Date</label>
																	<div class="controls">
																		<input type="text" id="input_date"
																			class="input-large ndshaatepicker-basic"
																			name="input_date">
																	</div>
																</div>

																<div class="span6 ">
																	<label class="control-label" for="hours">Select
																		Hour</label>
																	<div class="controls">
																		<select id="hours"
																			class="select2-select-00 input-large">
																			<option value="all">All</option>
																			<option value="00">12:00am to 12:59am</option>
																			<option value="01">01:00am to 01:59am</option>
																			<option value="02">02:00am to 02:59am</option>
																			<option value="03">03:00am to 03:59am</option>
																			<option value="04">04:00am to 04:59am</option>
																			<option value="05">05:00am to 05:59am</option>
																			<option value="06">06:00am to 06:59am</option>
																			<option value="07">07:00am to 07:59am</option>
																			<option value="08">08:00am to 08:59am</option>
																			<option value="09">09:00am to 09:59am</option>
																			<option value="10">10:00am to 10:59am</option>
																			<option value="11">11:00am to 11:59am</option>
																			<option value="12">12:00pm to 12:59pm</option>
																			<option value="13">01:00pm to 01:59pm</option>
																			<option value="14">02:00pm to 02:59pm</option>
																			<option value="15">03:00pm to 03:59pm</option>
																			<option value="16">04:00pm to 04:59pm</option>
																			<option value="17">05:00pm to 05:59pm</option>
																			<option value="18">06:00pm to 06:59pm</option>
																			<option value="19">07:00pm to 07:59pm</option>
																			<option value="20">08:00pm to 08:59pm</option>
																			<option value="21">09:00pm to 09:59pm</option>
																			<option value="22">10:00pm to 10:59pm</option>
																			<option value="23">11:00pm to 11:59pm</option>
																		</select>

																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="analytics-tab-content">
													<div id="demo-chart-01" style="height: 353px;"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</div>

		<footer id="footer">
			<div class="footer-left">
				<a><img src="assets/images/mobios_logo_index1.png" alt=""
					style="width: 100px; height: 26px;"></a>
			</div>
			<div class="footer-right">
				<p>Copyright 2016. All Rights Reserved.</p>
			</div>
		</footer>

	</div>

	<!-- Core Scripts -->
	<script src="assets/js/libs/jquery-1.8.3.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/js/libs/jquery.placeholder.min.js"></script>
	<script src="assets/js/libs/jquery.mousewheel.min.js"></script>

	<!-- Template Script -->
	<script src="assets/js/template.js"></script>
	<script src="assets/js/setup.js"></script>

	<!-- Customizer, remove if not needed -->
	<script src="assets/js/customizer.js"></script>

	<!-- Uniform Script -->
	<script src="plugins/uniform/jquery.uniform.min.js"></script>

	<!-- jquery-ui Scripts -->
	<script src="assets/jui/js/jquery-ui-1.9.2.min.js"></script>
	<script src="assets/jui/jquery-ui.custom.min.js"></script>
	<script src="assets/jui/timepicker/jquery-ui-timepicker.min.js"></script>
	<script src="assets/jui/jquery.ui.touch-punch.min.js"></script>

	<!-- Plugin Scripts -->

	<script src="plugins/flot/jquery.flot.js"></script>
	<script src="plugins/flot/plugins/jquery.flot.tooltip.min.js"></script>
	<script src="plugins/flot/plugins/jquery.flot.pie.min.js"></script>
	<script src="plugins/flot/plugins/jquery.flot.orderBars.min.js"></script>
	<script src="plugins/flot/plugins/jquery.flot.resize.min.js"></script>

	<!-- Demo Scripts  -->
	<script src="assets/js/demo/charts_rainco.js"></script>

	<!-- dashboard custom js -->
	<script src="dashboard/campaignDataFromMysqlDB_rainco.js"></script>

	<script src="assets/js/demo/dashboard_rainco.js"></script>
	<script src="assets/js/demo/ui_comps.js"></script>
	
	<script src="custom-plugins/circular-stat/circular-stat.min.js"></script>

</body>

</html>

